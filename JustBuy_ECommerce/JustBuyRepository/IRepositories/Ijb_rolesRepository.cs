﻿using JustBuyRepository.BaseRepository;
using JustBuyRepository.Context;
using JustBuyRepository.IBaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBuyRepository.IRepositories
{
    public class Ijb_rolesRepository<TE> : BaseRepository<TE>, IBaseRepository<TE>
        where TE : class
    {

        public Ijb_rolesRepository(JustBuyContext context) : base(context)
        {
        }

        public JustBuyContext JustBuyContext
        {
            get { return Context as JustBuyContext; }
        }

    }
}
