﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JustBuyRepository.IBaseRepository
{
    public interface IBaseRepository<TE> where TE : class
    {
        TE GetById(int id);
        IEnumerable<TE> GetAll();
        IEnumerable<TE> Find(Expression<Func<TE, bool>> predicate);

        TE SingleOrDefault(Expression<Func<TE, bool>> predicate);

        void Add(TE entity);
        void AddRange(IEnumerable<TE> entities);

        //I removed this because i want to do a logical delete
        //void Remove(TE entity);
        //void RemoveRange(IEnumerable<TE> entities);

    }
}
