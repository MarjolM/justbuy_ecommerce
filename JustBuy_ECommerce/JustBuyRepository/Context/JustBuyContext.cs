﻿using JustBuyModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBuyRepository.Context
{
    public class JustBuyContext : DbContext
    {
        public JustBuyContext() : base("JustBuyContext")
        {

        }

        public DbSet<jb_user> jb_users { get; set; }
        public DbSet<jb_role> jb_roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
