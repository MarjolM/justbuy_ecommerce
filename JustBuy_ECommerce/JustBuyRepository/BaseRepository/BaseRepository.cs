﻿using JustBuyRepository.IBaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JustBuyRepository.BaseRepository
{
    public class BaseRepository<TE> : IBaseRepository<TE> where TE : class
    {
        protected readonly DbContext Context;
        private readonly DbSet<TE> Entity;

        public BaseRepository(DbContext _context)
        {
            Context = _context;
            Entity = Context.Set<TE>();
        }
        public virtual void Add(TE entity)
        {
            Entity.Add(entity);
        }

        public virtual void AddRange(IEnumerable<TE> entities)
        {
            Entity.AddRange(entities);
        }

        public virtual IEnumerable<TE> Find(Expression<Func<TE, bool>> predicate)
        {
            return Entity.Where(predicate);
        }

        public virtual IEnumerable<TE> GetAll()
        {
            return Entity.ToList();
        }

        public virtual TE GetById(int id)
        {
            return Entity.Find(id);
        }


        public virtual TE SingleOrDefault(Expression<Func<TE, bool>> predicate)
        {
            return Entity.SingleOrDefault(predicate);
        }
    }
}
