﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBuyModel
{
    public class jb_role
    {
        public jb_role()
        {
            jb_users = new HashSet<jb_user>();
        }
        public int rl_id { get; set; }

        public string rl_role { get; set; }

        public bool rl_IsDeleted { get; set; }
        public DateTime? rl_dtIns { get; set; }
        public DateTime? rl_dtUpd { get; set; }

        public virtual ICollection<jb_user> jb_users { get; set; }
    }
}
