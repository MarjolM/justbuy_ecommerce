﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBuyModel
{
    public class jb_user
    {
        public int us_id { get; set; }
        public string us_name { get; set; }
        public string us_lastName { get; set; }
        public string us_email { get; set; }
        public byte[] us_password { get; set; }
        public bool us_attivo { get; set; }
        public DateTime? us_dtIns { get; set; }
        public DateTime? us_dtUpd { get; set; }

        public byte[] us_salt { get; set; }
       
        public int us_role { get; set; }
        public bool us_IsDeleted { get; set; }
        public jb_role com_role { get; set; }
    }
}
